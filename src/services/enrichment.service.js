import { httpService } from './http.service.js';

const END_POINT = 'enrich';

const cache = {};

export const enrichmentService = {
    enrichRow
}



async function enrichRow(row) {
    const cacheKey = row.SAMPLE_ID;
    if (cache[cacheKey]) return cache[cacheKey]
    const response = await httpService.post(END_POINT, row);
    cache[cacheKey] = response;
    return response;
}